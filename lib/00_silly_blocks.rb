def reverser
  words = yield.split
  words.map(&:reverse).join(' ')
end

def adder(num=1)
  yield + num
end

def repeater(repeats=1)
  repeats.times { yield }
end
